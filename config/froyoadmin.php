<?php

return [

	'user'=>[
		'_class'=>'thinkadmin\\base\\ThinkAdminAccount',
		'_config'=>[
			'homeUrl'=>'admin/common/index',
			'identifyClass'=>'app\\admin\\model\\bean\\ManagerBean',
			'authManager'=>[
				'_class'=>'thinkadmin\\base\\ThinkAdminAuthManager',
				'_roleModel'=>'app\\admin\\model\\bean\\RoleUserBean',
				'_accessModel'=>'app\\admin\\model\\bean\\AccessBean',
				'menuModelClass'=>'app\\admin\\model\\bean\\MenuModel',
				'superAdminId'=>1,
			]
		]
	],


];