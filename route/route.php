<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});


$runtime = CMF_ROOT.'/data/conf/route.php';
if(file_exists($runtime))
{
	$conf = require $runtime;
	foreach ($conf as $key => $value) {
		Route::rule($key,$value);
	}
}

Route::get('hello/:name', 'index/hello');

//设置插件入口路由

return [

];
