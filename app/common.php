<?php

function viewreplace($word){
	$replaces = config('view_replace_str');
	if(isset($replaces[$word]))
	{
		return $replaces[$word];
	}
	return $word;
}

function staticcdn($type,$url){

	if(strpos($url, 'http') ===0 || strpos($url, '//')===0)
	{
		return $url;
	}

	$asserts = [
		'css'=>[
			// 'bootstrap'=>'https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css',
			'bootstrap'=>viewreplace('__TMPL__').'/public/assets/themes/'.cmf_get_admin_style().'/bootstrap.min.css',
			'simplebootadmin'=>viewreplace('__TMPL__').'/public/assets/simpleboot3/css/simplebootadmin.css',
			'font-awesome'=>'https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.min.css',
			'login'=>viewreplace('__TMPL__').'/public/assets/themes/'.cmf_get_admin_style().'/login.css',
			'simplebootadminindex'=>viewreplace('__TMPL__').'/public/assets/themes/'.cmf_get_admin_style().'/simplebootadminindex.min.css',
		],
		'js'=>[
			'jquery'=>'https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js',
			'bootstrap'=>'https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js',
			'wind'=>viewreplace('__STATIC__').'/js/wind.js',
			'admin'=>viewreplace('__STATIC__').'/js/admin.js',
			'respond'=>'https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js',
			'adminindex'=>viewreplace('__TMPL__').'/public/assets/simpleboot3/js/adminindex.js',

			'ueditor.config'=>viewreplace('__STATIC__').'/js/ueditor/ueditor.config.js',
			'ueditor.all'=>viewreplace('__STATIC__').'/js/ueditor/ueditor.all.min.js',
		]
		
	];
	$index = strpos( $url, '?');
	$params = '';

	if($index)
	{
		$name = substr($url, 0,$index);
		$params = substr($url, $index+1);
	}
	else
	{
		$name = $url;
	}

	if(isset($asserts[$type][ $name ]))
	{
		return $asserts[$type][ $name ].'?'.$params;
	}
	return $url;
}