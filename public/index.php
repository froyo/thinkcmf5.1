<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 老猫 <zxxjjforever@163.com>
// +----------------------------------------------------------------------

// [ 入口文件 ]

namespace think;


// 调试模式开关
define("APP_DEBUG", true);

// // 定义CMF根目录,可更改此目录
define('CMF_ROOT', __DIR__ . '/../');

// // 定义应用目录
define('APP_PATH', CMF_ROOT . 'app/');

// // 定义CMF核心包目录
define('CMF_PATH', CMF_ROOT . 'simplewind/cmf/');

// // 定义插件目录
define('PLUGINS_PATH', __DIR__ . '/plugins/');

// // 定义扩展目录
define('EXTEND_PATH', CMF_ROOT . 'simplewind/extend/');
define('VENDOR_PATH', CMF_ROOT . 'simplewind/vendor/');

// // 定义应用的运行时目录
define('RUNTIME_PATH', CMF_ROOT . 'data/runtime/');

// // 定义CMF 版本号
define('THINKCMF_VERSION', '5.1.0');

define('EXT', '.php');
define('THINK_PATH',CMF_ROOT . 'simplewind/thinkphp/');
define('DS',DIRECTORY_SEPARATOR);
define('IS_WIN', strpos(PHP_OS, 'WIN') !== false);
define('IS_CLI', PHP_SAPI == 'cli' ? true : false);
$documentroot = $_SERVER['DOCUMENT_ROOT'];
$scriptname = $_SERVER['SCRIPT_FILENAME'];
$tmp = str_replace($documentroot, '', $scriptname);
$root = substr($tmp, 0,strrpos($tmp,"/"));

defined('__STATIC__') or define('__STATIC__',$root.'/static');
defined('__ROOT__') or define('__ROOT__',$root);
// // 加载框架基础文件
require CMF_ROOT . 'simplewind/thinkphp/base.php';

// // 执行应用
// \think\App::run()->send();



// 执行应用并响应
Container::get('app',['appPath'=>APP_PATH])->run()->send();